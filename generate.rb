require 'erb'

session = ARGV[0]

if session.nil? || session.empty?
  puts "Provide a session name"
  return 1
end

cmd = 'tmux list-windows -t ' + session.to_s + ' -F "window: \'#{window_name}\', panes: \'#{window_panes}\'"'

windows = %x(#{cmd}).split("\n")
return 1 if windows.count == 0

result = []
windows.each do |window|
  data = window.match(/window: '(?<window_name>.*)', panes: '(?<panes_count>\d+)'/)
  result << {
    name: data['window_name'],
    panes: data['panes_count'].to_i
  }
end

file_name = "#{session}.yml"
File.write(file_name, ERB.new(File.read('template.erb')).result(binding))

puts "Write the config file"
puts "#{Dir.pwd}/#{session}.yml"
