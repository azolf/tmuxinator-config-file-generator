Have you ever worked with tmux using tmuxinator? You have to write your own configuration files for each session you want. What if you have a running tmux session with lots of windows and panes? It takes some time to generate your config file by hand. In this article, I’m going to create a script that will generate a tmuxinator configuration file from a running tmux session.

[Read more about it here](https://azolf.medium.com/tmuxinator-config-file-generator-41b93fa03728)
